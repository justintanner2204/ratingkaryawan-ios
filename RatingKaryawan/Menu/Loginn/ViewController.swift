import UIKit

struct UserResponse: Codable {
    let status: Int
    let user: User
    
    private enum CodingKeys: String, CodingKey {
        case status
        case user = "user"
    }
}

struct User: Codable {
    let command: String
    let rowCount: Int
    let oid: Int?
    let rowCtor: Int?
    let rowAsArray: Bool
    let rows: [Rows]
    let fields: [Fields]
    let parsers: [Parsers]?
    let types: Type
    
    private enum CodingKeys: String, CodingKey {
        case command
        case rowCount
        case oid
        case rowCtor = "RowCtor"
        case rowAsArray
        case rows
        case fields
        case parsers = "_parsers"
        case types = "_types"
    }
}

struct Rows: Codable {
    let userId: Int
    let uName: String
    let email: String
    let divName: String
    let url: String
    
    private enum CodingKeys: String, CodingKey {
        case userId = "user_id"
        case uName = "u_name"
        case email
        case divName = "div_name"
        case url
    }
}

struct Fields: Codable {
    let name: String
    let tableID: Int
    let columnID: Int
    let datatypeID: Int?
    let dataTypeSize: Int
    let dataTypeModifier: Int
    let format: String
    
    private enum CodingKeys: String, CodingKey {
        case name
        case tableID
        case columnID
        case datatypeID
        case dataTypeSize
        case dataTypeModifier
        case format
    }
}

struct Parsers: Codable {
    
}

struct Types: Codable {
    let types: Type?
    let text: Text?
    let binary: Binary?
    
    private enum CodingKeys: String, CodingKey {
        case types = "_types"
        case text
        case binary
    }
}

struct Type: Codable {
    
}

struct Text: Codable {
    
}

struct Binary: Codable {
    
}

class ViewController: UIViewController {
    
    
    private var datauser : [Rows] = []

    @IBOutlet weak var UserName: UITextField!
    
    @IBOutlet weak var Password: UITextField!
    
    @IBAction func LoginBtn(_ sender: Any) {
        
        if UserName.text!.isEmpty || Password.text!.isEmpty {
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            let alert = UIAlertController(title: "Login Fail", message: "Incorect Username or Password", preferredStyle: .alert)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
        else{
            Login()
        }
}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
    }
    
    //buat ngambil func di appDelegate
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    
//    func LoginSequee() {
//        // percobaan 1
//        
////       let storyboard = UIStoryboard(name: "HomePage", bundle: nil)
////        let HomePageController = storyboard.instantiateViewController(withIdentifier: "navBar") as! HomePageController
////        self.present(HomePageController, animated: true, completion: nil)
//        
//        //percobaan 2
//        
//        let profileController = self.storyboard?.instantiateViewController(withIdentifier: "HomePageController") as! HomePageController
//        self.navigationController?.pushViewController(profileController, animated: true)
//
//        self.dismiss(animated: false, completion: nil)
//    }
    
    func Login() {
        let user = UserName.text
        let pass = Password.text
        
        let json = "http://rating.immobisp.com/getlogin/\(user!)/\(pass!)"
        
        guard let myUrl = URL(string: json) else { return }
        
        URLSession.shared.dataTask(with: myUrl) { (data, response, error) in
            guard let data = data else {return}
        
            do{
                let userLog = try JSONDecoder().decode(UserResponse.self, from: data)
                print("this is the json\(userLog.user.rows)")
                GlobalVariable.id = String(userLog.user.rows[0].userId) // ini buat ngambil user id
                GlobalVariable.userName = String(userLog.user.rows[0].uName) // ini ngambil userName
                DispatchQueue.main.async {
                    self.appDelegate.loginSegue()
                } 
            }catch{
                print("ini errornya lohh 🤪 \(error)")
                let action = UIAlertAction(title: "ok", style: .default, handler: nil)
                let alert = UIAlertController(title: "Login Fail...", message: "Incorect Email/Password", preferredStyle: .alert)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
        }.resume()
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}


