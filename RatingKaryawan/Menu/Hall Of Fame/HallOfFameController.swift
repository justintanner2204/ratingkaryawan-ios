//
//  HallOfFameController.swift
//  RatingKaryawan
//
//  Created by Poernomo Santoso  on 08/11/19.
//  Copyright © 2019 Immobi. All rights reserved.
//

import UIKit
import Cosmos


struct DataModel: Codable {
    let nama: [String]
    let uId: [Int]
    let totalakhir: [Double]
    private enum CodingKeys: String, CodingKey {
        case nama
        case uId = "u_id"
        case totalakhir
    }
}



class HallOfFameController: UIViewController {
    
    var dataHOF = [String]()
    

    @IBOutlet weak var TableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        json()
        // Do any additional setup after loading the view.
    }
    
    func json() {
        let json = "http://rating.immobisp.com/gethalloffame"
        guard let url = URL(string: json) else {return}
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else {return}
            
            do{
                let parse = try JSONDecoder().decode(DataModel.self, from: data)
                print("ini parse namanya...🤪 \(parse)")
                self.dataHOF = [parse].map({$0.nama}).compactMap(String.init)
//                self.dataHOF = [parse]
                print("ini data HOF🏊🏻‍♂️ \(self.dataHOF)")
                DispatchQueue.main.async {
                    self.TableView.reloadData()
                }
            }catch{
                print("ini errornya lohh 🧐 \(error)")
            }
        }.resume()
    }
    
}


extension HallOfFameController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataHOF.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dats = dataHOF[indexPath.row]
        guard let cell = TableView.dequeueReusableCell(withIdentifier: "Cell") as? HallOfFameCell else {return UITableViewCell()}
        
        cell.nameLbl.text = dats
        
        print("ini data di tableview ✌🏻 \(dats)")
        
        return cell
    }
    
    
}
