import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var ProfilePic: UIImageView!
    @IBOutlet weak var NIKLabel: UILabel!
    @IBOutlet weak var namaLabel: UILabel!
    @IBOutlet weak var posLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.makingRoundedImageProfileWithRoundedBorder()
        
        self.namaLabel.text = GlobalVariable.userName
        self.posLabel.text = GlobalVariable.divisiUserLog
        self.NIKLabel.text = GlobalVariable.idLogin
        
    }
    
    private func makingRoundedImageProfileWithRoundedBorder() {

        self.ProfilePic.layer.cornerRadius = 20.0
        
        self.ProfilePic.clipsToBounds = true
        
        self.ProfilePic.layer.borderWidth = 10.0
        self.ProfilePic.layer.borderColor = UIColor.white.cgColor
    }
}
