import UIKit

struct Commend : Codable {

    let command : String
    let rowCount : Int
    let oid : Int?
    let rows : [Rows2]
    
    enum CodingKeys : String, CodingKey {
        case command
        case rowCount
        case oid
        case rows = "rows"
    }
}

struct Rows2 : Codable {
    
    let user_id : Int
    let u_name : String?
    let div_name : String
}

class RateViewViewController: UIViewController {
    
    @IBOutlet weak var rateTable: UITableView!

    final let url = "http://rating.immobisp.com/userlist"
    
    private var temp : [Rows2] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        donwloadJson()
    }
    
    func donwloadJson(){
        
        guard let downloadURL = URL(string: url) else { return }
        
        URLSession.shared.dataTask(with: downloadURL) { data, urlResponse, error in
            guard let data = data
            else { return }
            print("downloaded")
            do{
                let decoder = JSONDecoder()
                let karyawans =  try decoder.decode(Commend.self, from: data)
                self.temp = [karyawans].flatMap{$0.rows}
                print(karyawans)
                DispatchQueue.main.async {
                    self.rateTable.reloadData()
                }
            } catch{
             print("\(error)")
            }
        }.resume()
    }
}

extension RateViewViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return temp.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = temp[indexPath.row]
        
        guard let cell = rateTable.dequeueReusableCell(withIdentifier: "Cell") as? RateEmployeeCell else {return UITableViewCell()}
        
        cell.nameLbl.text = data.u_name
        cell.divLbl.text = data.div_name
        
        return cell
    }
}
