//
//  BawahanController.swift
//  RatingKaryawan
//
//  Created by Poernomo Santoso  on 22/11/19.
//  Copyright © 2019 Immobi. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD


struct DataBawahan : Codable {
    let command : String
    let rowCount : Int
    let oid : String?
    let rows : [RowsBawahan]
    let fields : [FieldsBawahan]
    let _parsers : [ParsersBawahan]
    let _types : TypesBawahan
    let RowCtor : String?
    let rowAsArray : Bool
}

struct RowsBawahan : Codable{
    let u_name : String
    let user_id : Int
    let div_name : String
    let flag : Int
}

struct FieldsBawahan : Codable {
    let name : String
    let tableID : Int
    let columnID : Int
    let dataTypeID : Int
    let dataTypeSize : Int
    let dataTypeModifier : Int
    let format : String
}

struct ParsersBawahan : Codable {
    
}

struct TypesBawahan: Codable {
    let types: Type2?
    let text: Text2?
    let binary: Binary2?
    
    private enum CodingKeys: String, CodingKey {
        case types = "_types"
        case text
        case binary
    }
}

struct TypeBawahan : Codable{
    
}

struct TextBawahan: Codable {
    
}

struct BinaryBawahan: Codable {
    
}




class BawahanController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        json()
        // Do any additional setup after loading the view.
    }
    
    private var dataBawahan : [RowsBawahan] = []
    
    func json() {
        
        let api = "http://rating.immobisp.com/mychild/\(GlobalVariable.idLogin)"
        guard let url = URL(string: api) else {return}
        
        URLSession.shared.dataTask(with: url) { (data, resp, err) in
            guard let data = data else {return}
            
            do{
                let parse = try JSONDecoder().decode(DataBawahan.self, from: data)
                self.dataBawahan = [parse].flatMap{$0.rows}
                
                DispatchQueue.main.async {
                    
                    if self.dataBawahan.isEmpty {
                        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
                        let alert = UIAlertController(title: "Tidak ada data Bawahan", message: "No Data...", preferredStyle: .alert)
                        alert.addAction(action)
                        self.present(alert, animated: true, completion:  nil)
                        SVProgressHUD.dismiss()
                    }else {
                        self.tableView.reloadData()
                        SVProgressHUD.dismiss()
                    }
                }
            }catch{
                print("ini errornya 😈 \(err)")
            }
        }.resume()
    }
    
}

extension BawahanController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataBawahan.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = dataBawahan[indexPath.row]
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? BawahanCell else {return UITableViewCell()}
        
        cell.namaLbl.text = data.u_name
        cell.jabatanLbl.text = data.div_name
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = dataBawahan[indexPath.row]
        
        GlobalVariable.idSekawan = String(data.user_id)
        GlobalVariable.divisiName = String(data.div_name)
        GlobalVariable.userName = String(data.u_name)
        
        performSegue(withIdentifier: "Segue", sender: data)
    }
    
    
}
