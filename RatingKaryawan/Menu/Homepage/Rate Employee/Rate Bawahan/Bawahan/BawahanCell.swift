//
//  BawahanCell.swift
//  RatingKaryawan
//
//  Created by Poernomo Santoso  on 22/11/19.
//  Copyright © 2019 Immobi. All rights reserved.
//

import UIKit

class BawahanCell: UITableViewCell {

    @IBOutlet weak var rateLbl: UILabel!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var namaLbl: UILabel!
    @IBOutlet weak var jabatanLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
