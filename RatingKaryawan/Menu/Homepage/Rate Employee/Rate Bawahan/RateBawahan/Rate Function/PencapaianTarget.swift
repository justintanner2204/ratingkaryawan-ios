//
//  PencapaianTarget.swift
//  RatingKaryawan
//
//  Created by Poernomo Santoso  on 24/11/19.
//  Copyright © 2019 Immobi. All rights reserved.
//

import UIKit
import Alamofire
import Cosmos

class PencapaianTarget: UIViewController {

    @IBOutlet weak var textField: UITextView!
       @IBOutlet weak var rate: CosmosView!
       @IBOutlet weak var divisiLbl: UILabel!
       @IBOutlet weak var namaLbl: UILabel!
       
       
       
       @IBAction func submitBtn(_ sender: Any) {
           
           if ratings.isEmpty {
               let action = UIAlertAction(title: "ok", style: .default, handler: nil)
               let alert = UIAlertController(title: "Tolong diisi kolom bintang", message: "Kolom Bintang tidak boleh kosong", preferredStyle: .alert)
               alert.addAction(action)
               self.present(alert, animated: true, completion: nil)
           } else if textField.text.isEmpty{
               let action = UIAlertAction(title: "ok", style: .default, handler: nil)
               let alert = UIAlertController(title: "Tolong diisi kolom Komentar", message: "Kolom Komentar kosong", preferredStyle: .alert)
               alert.addAction(action)
               self.present(alert, animated: true, completion: nil)
           }else {
               postJson()
           }
       }
       
       
       private var ratings = [Double]()
       
       override func viewDidLoad() {
           super.viewDidLoad()
           
           rate.didTouchCosmos = { ratings in
               self.ratings = [ratings]
               
           }

           namaLbl.text = GlobalVariable.userName
           divisiLbl.text = GlobalVariable.divisiName
           // Do any additional setup after loading the view.
       }
       


       func postJson() {
       
           let komen : String = textField.text!
           let rate = ratings.reduce(0, +)
           
           let url = "http://rating.immobisp.com/insertratingtochild/906/\(rate)/\(komen)/\(GlobalVariable.idSekawan)/\(GlobalVariable.idLogin)"
           
           guard let urlEncoded = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {return}
           
           Alamofire.request(urlEncoded, method: .post, parameters: nil).responseJSON { response in
               switch (response.result) {
               case .success :
                   print("Ini responds nya 🤗 \(response)")
                   print("ini urlnya \(url)")
                   let action = UIAlertAction(title: "ok", style: .default, handler: nil)
                              let alert = UIAlertController(title: "Rate Success", message: "Terima Kasih sudah memberi rating ", preferredStyle: .alert)
                              alert.addAction(action)
                              self.present(alert, animated: true, completion: nil)
                   break
               case .failure :
                   print(Error.self)
                   print("respons nya 😡\(response)")
                   print("ini url nya🤬 \(url)")
               }
           }
           
       }
   }

