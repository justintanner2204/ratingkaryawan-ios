//
//  RateBawahanController.swift
//  RatingKaryawan
//
//  Created by Poernomo Santoso  on 24/11/19.
//  Copyright © 2019 Immobi. All rights reserved.
//

import UIKit

class RateBawahanController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.nameLbl.text = GlobalVariable.userName
        self.divisionLbl.text = GlobalVariable.divisiName

        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var nameLbl: UILabel!
    
    @IBOutlet weak var divisionLbl: UILabel!
   

}
