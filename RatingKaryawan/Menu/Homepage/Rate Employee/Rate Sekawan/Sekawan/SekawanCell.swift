//
//  SekawanCell.swift
//  RatingKaryawan
//
//  Created by Poernomo Santoso  on 16/10/19.
//  Copyright © 2019 Immobi. All rights reserved.
//

import UIKit

class SekawanCell: UITableViewCell {
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var divisionLbl: UILabel!
    @IBOutlet weak var rateBtn: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
