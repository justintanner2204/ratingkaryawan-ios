//
//  SekawanController.swift
//  RatingKaryawan
//
//  Created by Poernomo Santoso  on 16/10/19.
//  Copyright © 2019 Immobi. All rights reserved.
//

import UIKit
import SVProgressHUD

struct Data : Codable {
    let command : String
    let rowCount : Int
    let oid : String?
    let rows : [Rows3]
    let fields : [Fields3]
    let _parsers : [Parsers3]
    let _types : Types2
    let RowCtor : String?
    let rowAsArray : Bool
}

struct Rows3 : Codable{
    let u_name : String
    let user_id : Int
    let div_name : String
    let flag : Int
}

struct Fields3 : Codable {
    let name : String
    let tableID : Int
    let columnID : Int
    let dataTypeID : Int
    let dataTypeSize : Int
    let dataTypeModifier : Int
    let format : String
}

struct Parsers3 : Codable {
    
}

struct Types2: Codable {
    let types: Type2?
    let text: Text2?
    let binary: Binary2?
    
    private enum CodingKeys: String, CodingKey {
        case types = "_types"
        case text
        case binary
    }
}

struct Type2 : Codable{
    
}

struct Text2: Codable {
    
}

struct Binary2: Codable {
    
}

class SekawanController: UIViewController {
    
    private var dataKaryawan : [Rows3] = []
    

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        json()
        // Do any additional setup after loading the view.
    }
    
    func json() {
        
        Loading()
        
        let api = "http://rating.immobisp.com/myfriend/\(GlobalVariable.idLogin)"
        guard let url = URL(string: api) else {return}
        
        URLSession.shared.dataTask(with: url) { (data, respon, error) in
            guard let data = data else {return}
            
            do{
                let parse = try JSONDecoder().decode(Data.self, from: data)
                self.dataKaryawan = [parse].flatMap{$0.rows}
                print("Ini data karyawannya 🤪 \(self.dataKaryawan)")
                DispatchQueue.main.async {
                    if self.dataKaryawan.isEmpty{
                        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
                        let alert = UIAlertController(title: "Tidak ada data Sekawan", message: "No Data...", preferredStyle: .alert)
                        alert.addAction(action)
                        self.present(alert, animated: true, completion:  nil)
                        SVProgressHUD.dismiss()
                    }else{
                    self.tableView.reloadData()
                        SVProgressHUD.dismiss()
                    }
                }
            }catch{
                print(error)
            }
        }.resume()
    }

}

extension SekawanController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataKaryawan.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? SekawanCell else {return UITableViewCell()}
        let data = dataKaryawan[indexPath.row]
        
        cell.nameLbl.text = data.u_name
        cell.divisionLbl.text = data.div_name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dataId = dataKaryawan[indexPath.row]
        GlobalVariable.idSekawan = String(dataId.user_id)
        GlobalVariable.divisiName = String(dataId.div_name)
        GlobalVariable.userName = String(dataId.u_name)
        
        performSegue(withIdentifier: "Segue", sender: dataId)
    }
}
