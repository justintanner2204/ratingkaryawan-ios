//
//  RateSekawanContoller.swift
//  RatingKaryawan
//
//  Created by Poernomo Santoso  on 16/10/19.
//  Copyright © 2019 Immobi. All rights reserved.
//

import UIKit
import Cosmos


@IBDesignable extension UIView{
    
    @IBInspectable var borderWidthh : CGFloat{
        set {
            layer.borderWidth = newValue
        }
        get{
            return layer.borderWidth
        }
    }
    @IBInspectable var cornerRadiuss : CGFloat{
        set{
            layer.cornerRadius = newValue
        }
        get{
            return layer.cornerRadius
        }
    }
    @IBInspectable var borderColorr: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}

class RateSekawanContoller: UIViewController {
    
    
    
    @IBOutlet weak var KomunikasiBtn: UIButton!
    
    @IBOutlet weak var KerapihanBtn: UIButton!
    
    
    @IBOutlet weak var imgKarwayan: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var divisionLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.divisionLbl.text = GlobalVariable.divisiName
        self.nameLbl.text = GlobalVariable.userName
        
    

        // Do any additional setup after loading the view.
    }
    
    func postJson() {
        
        let url = ""
        
    }

}
