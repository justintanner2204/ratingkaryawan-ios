//
//  SekawanKerapihanController.swift
//  RatingKaryawan
//
//  Created by Poernomo Santoso  on 21/11/19.
//  Copyright © 2019 Immobi. All rights reserved.
//

import UIKit
import Cosmos
import Alamofire

class SekawanKerapihanController: UIViewController {

    
    @IBOutlet weak var profilePic: UIImageView!
    
    @IBOutlet weak var NameLbl: UILabel!
    
    @IBOutlet weak var JabatanLbl: UILabel!
    
    @IBOutlet weak var RateCosmos: CosmosView!
    
    @IBOutlet weak var CommentTxt: UITextView!
    
    
    
    private var ratings = [Double]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        RateCosmos.didTouchCosmos = { rating in
            print("Ini rate nya 😬 \(rating)")
            
            self.ratings = [rating]
        }
        
        NameLbl.text = GlobalVariable.userName
        JabatanLbl.text = GlobalVariable.divisiName

        // Do any additional setup after loading the view.
    }
    
    @IBAction func SubmitBtn(_ sender: Any) {
        
        if ratings.isEmpty {
        
            let action = UIAlertAction(title: "ok", style: .default, handler: nil)
                       let alert = UIAlertController(title: "Tolong diisi kolom bintang", message: "Kolom Bintang tidak boleh kosong", preferredStyle: .alert)
                       alert.addAction(action)
                       self.present(alert, animated: true, completion: nil)
            
        }else if CommentTxt.text.isEmpty {
             let action = UIAlertAction(title: "ok", style: .default, handler: nil)
            let alert = UIAlertController(title: "Tolong diisi kolom Komentar", message: "Kolom Komentar kosong", preferredStyle: .alert)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }else {
            postJson()
        }
        
    }
    
    func postJson() {
        
        
        let komen : String = CommentTxt.text!
        let Rate = ratings.reduce(0, +)
        
        let url = "http://rating.immobisp.com/insertratingtofriend/910/\(Rate)/\(komen)/\(GlobalVariable.idSekawan)/\(GlobalVariable.idLogin)"
        
        
        guard let urlEncoded = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {return}
        
        Alamofire.request(urlEncoded, method: .post, parameters: nil).responseJSON { responds in
            switch (responds.result) {
            case .success :
                print("Ini responds nya 🤗 \(responds)")
                print("ini urlnya \(url)")
                let action = UIAlertAction(title: "ok", style: .default, handler: nil)
                           let alert = UIAlertController(title: "Rate Success", message: "Terima Kasih sudah memberi rating ", preferredStyle: .alert)
                           alert.addAction(action)
                           self.present(alert, animated: true, completion: nil)
                break
            case .failure :
                print(Error.self)
                print("respons nya 😡\(responds)")
                print("ini url nya🤬 \(url)")
            }
        }
    }
    

    

}
