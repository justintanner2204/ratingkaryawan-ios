//
//  SekawanKomunikasiController.swift
//  RatingKaryawan
//
//  Created by Poernomo Santoso  on 21/11/19.
//  Copyright © 2019 Immobi. All rights reserved.
//

import UIKit
import Cosmos
import Alamofire

class SekawanKomunikasiController: UIViewController {

    @IBOutlet weak var ProfilePic: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var JabatanLbl: UILabel!
    
    @IBOutlet weak var textField: UITextView!
    
    @IBOutlet weak var rateCosmos: CosmosView!

    
    private var ratings = [Double]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLbl.text = GlobalVariable.userName
        JabatanLbl.text = GlobalVariable.divisiName
        
        
        //ini buat ngambil value dari rate starnnya yg ntar dimasukin ke dalem parameter api
        rateCosmos.didTouchCosmos = { rating in
            print("ini rate nya 😈 \(rating)")
            
            self.ratings = [rating]
        }
    }
    
    
    
    func postJson() {
        
        let komen : String = textField.text!
        let Rate = ratings.reduce(0, +)
        
        let url = "http://rating.immobisp.com/insertratingtofriend/909/\(Rate)/\(komen)/\(GlobalVariable.idSekawan)/\(GlobalVariable.idLogin)"
        
        
        //ini biar bisa ngebaca parameter kalo ada spasi di parameternya
        guard let urlEncoded = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            else {return}
        
        //ini buat parsing apinya, di post dan dgn encode `
        Alamofire.request(urlEncoded, method: .post, parameters: nil).responseJSON { responds in
            switch (responds.result) {
            case .success :
                print("Ini responds nya 🤗 \(responds)")
                print("ini urlnya \(url)")
                let action = UIAlertAction(title: "ok", style: .default, handler: nil)
                           let alert = UIAlertController(title: "Rate Success", message: "Terima Kasih sudah memberi rating ", preferredStyle: .alert)
                           alert.addAction(action)
                           self.present(alert, animated: true, completion: nil)
                break
            case .failure :
                print(Error.self)
                print("respons nya 😡\(responds)")
                print("ini url nya🤬 \(url)")
            }
        }
    }
    
    @IBAction func submitBtn(_ sender: Any) {
        
        
        if ratings.isEmpty {
        
            let action = UIAlertAction(title: "ok", style: .default, handler: nil)
                       let alert = UIAlertController(title: "Tolong diisi kolom bintang", message: "Kolom Bintang tidak boleh kosong", preferredStyle: .alert)
                       alert.addAction(action)
                       self.present(alert, animated: true, completion: nil)
            
        }else if textField.text.isEmpty {
             let action = UIAlertAction(title: "ok", style: .default, handler: nil)
            let alert = UIAlertController(title: "Tolong diisi kolom Komentar", message: "Kolom Komentar kosong", preferredStyle: .alert)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }else {
            postJson()
        }
        
        
    }
    

}
