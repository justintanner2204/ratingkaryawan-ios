//
//  KepemimpinanController.swift
//  RatingKaryawan
//
//  Created by Poernomo Santoso  on 05/12/19.
//  Copyright © 2019 Immobi. All rights reserved.
//

import UIKit
import Cosmos
import Alamofire

class KepemimpinanController: UIViewController {

    @IBOutlet weak var divisiLbl: UILabel!
    @IBOutlet weak var namaLbl: UILabel!
    
    @IBOutlet weak var rateCosmos: CosmosView!
    
    @IBOutlet weak var textField: UITextView!
    
    @IBAction func submitBtnn(_ sender: Any) {
        
        if ratings.isEmpty {
            
            let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
            let alert = UIAlertController(title: "tolong diisi kolom bintang", message: "Kolom Bintang tidak boleh kosong", preferredStyle: .alert)
            alert.addAction(action)
            self.present(alert,animated: true, completion: nil)
        }else if textField.text.isEmpty{
            
            let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
            let alert = UIAlertController(title: "tolong diisi kolom komentar", message: "Kolom komentar tidak bolehk kosong", preferredStyle: .alert)
            
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }else {
            postJson()
        }
        
        
    }
    
    private var ratings = [Double]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        rateCosmos.didTouchCosmos = { rating in
            print("ini rate nya \(rating)")
            
            self.ratings = [rating]
        }
        
        self.divisiLbl.text = GlobalVariable.divisiName
        self.namaLbl.text = GlobalVariable.userName

        // Do any additional setup after loading the view.
    }
    
    
    
    func postJson() {
        let komen : String = textField.text!
        let Rate = ratings.reduce(0, +)
        
        let url = "http://rating.immobisp.com/insertratingtoparent/908/\(Rate)/\(komen)/\(GlobalVariable.idSekawan)/\(GlobalVariable.idLogin)"
        
        
        guard let urlEncoded = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {return}
        
        Alamofire.request(urlEncoded, method: .post, parameters: nil).responseJSON { responds in
            switch (responds.result) {
            case .success :
                print("Ini responds nya 🤗 \(responds)")
                print("ini urlnya \(url)")
                let action = UIAlertAction(title: "ok", style: .default, handler: nil)
                           let alert = UIAlertController(title: "Rate Success", message: "Terima Kasih sudah memberi rating ", preferredStyle: .alert)
                           alert.addAction(action)
                           self.present(alert, animated: true, completion: nil)
                break
            case .failure :
                print(Error.self)
                print("respons nya 😡\(responds)")
                print("ini url nya🤬 \(url)")
            }
        }
    }


}
