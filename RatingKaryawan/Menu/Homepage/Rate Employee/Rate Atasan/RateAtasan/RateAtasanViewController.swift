import UIKit

class RateAtasanViewController: UIViewController {

    @IBOutlet weak var namaLbl: UILabel!
    
    @IBOutlet weak var divisiLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.namaLbl.text = GlobalVariable.userName
        self.divisiLbl.text = GlobalVariable.divisiName
    }
    
    
    
}
