import UIKit
import SVProgressHUD

struct Names : Codable {
    
    let command : String
    let rowCount : Int
    let oid : Int?
    let rows : [Rows1]
    
    enum CodingKeys : String, CodingKey {
        case command
        case rowCount
        case oid
        case rows = "rows"
    }
}

struct Rows1 : Codable {
    
    let user_id : Int
    let u_name : String
    let div_name : String
}

class AtasanViewController: UIViewController {
    
    @IBOutlet weak var atasanTable: UITableView!
    
    final let url = "http://rating.immobisp.com/myparent/\(GlobalVariable.idLogin)"
    
    private var temp : [Rows1] = []
    
        override func viewDidLoad() {
            super.viewDidLoad()
            viewJson()
            
    }
    
    func viewJson(){
        
        guard let downloadURL = URL(string: url) else { return }
        
        URLSession.shared.dataTask(with: downloadURL) { data, urlResponse, error in
            guard let data = data
                else { return }
            print("downloaded")
            do{
                let decoder = JSONDecoder()
                let karyawans = try decoder.decode(Names.self, from: data)
                self.temp = [karyawans].flatMap{$0.rows}
                print(karyawans)
                DispatchQueue.main.async {
                    if self.temp.isEmpty{
                        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
                        let alert = UIAlertController(title: "Tidak ada data atasan", message: "No Data...", preferredStyle: .alert)
                        alert.addAction(action)
                        self.present(alert, animated: true, completion:  nil)
                        SVProgressHUD.dismiss()
                    }else {
                        self.atasanTable.reloadData()
                        SVProgressHUD.dismiss()
                    }
                }
            } catch{
                print("\(error)")
            }
        }.resume()
    }
}
    
extension AtasanViewController: UITableViewDataSource, UITableViewDelegate{
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return temp.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let data = temp[indexPath.row]
            
            guard let cell = atasanTable.dequeueReusableCell(withIdentifier: "Cell") as? AtasanCell else {return UITableViewCell()}
            
            cell.nameLabel.text = data.u_name
            cell.divisionLbl.text = data.div_name
            
            
            return cell
        }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = temp[indexPath.row]
        
        GlobalVariable.idSekawan = String(data.user_id)
        GlobalVariable.divisiName = String(data.div_name)
        GlobalVariable.userName = String(data.u_name)
        
        performSegue(withIdentifier: "Segue", sender: data)
        
    }
}
