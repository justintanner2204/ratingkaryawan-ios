//
//  HallOfFameCell.swift
//  RatingKaryawan
//
//  Created by Poernomo Santoso  on 20/11/19.
//  Copyright © 2019 Immobi. All rights reserved.
//

import UIKit
import Cosmos 

class HallOfFameCell: UITableViewCell {

    @IBOutlet weak var nameLbl: UILabel!
    
    @IBOutlet weak var RateStar: CosmosView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
