//
//  HallOfFameController.swift
//  RatingKaryawan
//
//  Created by Poernomo Santoso  on 20/11/19.
//  Copyright © 2019 Immobi. All rights reserved.
//

import UIKit
import Cosmos


struct DataModel : Codable {
    let nama : [String]
    let uID : [Int]
    let totalAkhir : [Double]
    
    private enum CodingKeys : String, CodingKey {
        case nama
        case uID = "u_id"
        case totalAkhir
    }
}



class HallOfFameController: UIViewController {

    @IBOutlet weak var HOFTableView: UITableView!
    
    private var dataHOF = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        json()
        // Do any additional setup after loading the view.
    }
    
    func json() {
        let json = "http://rating.immobisp.com/gethalloffame"
        guard let Murl = URL(string: json) else {return}
        
        URLSession.shared.dataTask(with: Murl) { (data, resp, err) in
            guard let data = data else {return}
            
            do{
                let parse = try JSONDecoder().decode(DataModel.self, from:  data)
                self.dataHOF = [parse].map({$0.nama}).compactMap(String.init)
                print("😈 \(self.dataHOF)")
                DispatchQueue.main.async {
                    self.HOFTableView.reloadData()
                }
            }catch{
                print ("Ini errornya yaa 😬 \(err)")
            }
        }.resume()
    }
}


extension HallOfFameController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataHOF.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = dataHOF[indexPath.row]
        
        guard let cell = HOFTableView.dequeueReusableCell(withIdentifier: "Cell") as? HallOfFameCell else {return UITableViewCell()}
        
        cell.nameLbl.text = data
        
        return cell
    }
    
    
}
