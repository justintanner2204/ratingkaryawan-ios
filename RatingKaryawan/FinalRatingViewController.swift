import UIKit

//class FinalRatingViewController: UIViewController {

    struct Commended : Codable {
        
        let command : String
        let rowCount : Int
        let oid : Int?
        let Rows : [rows]
        
        enum CodingKeys : String, CodingKey {
            case command
            case rowCount
            case oid
            case Rows = "rows"
        }
    }
    
    struct rowse : Codable {
        
        let user_id : Int
        let u_name : String
        let div_name : String
    }

class FinalRatingViewController: UIViewController{
        
        @IBOutlet weak var FinalRating: UITableView!
        
        final let url = "http://rating.immobisp.com/userlist"
    
        private var temp : [rows] = []
    
        override func viewDidLoad() {
        super.viewDidLoad()
        downloadJson()
    }
    
    func downloadJson(){
        guard let downloadURL = URL(string: url) else { return }
        
        URLSession.shared.dataTask(with: downloadURL) { data, urlResponse, error in
            guard let data = data
                else { return }
            print("downloaded")
            do{
                let decoder = JSONDecoder()
                let karyawans = try decoder.decode(Commend.self, from: data)
                self.temp = [karyawans].flatMap{$0.Rows}
                print(karyawans)
                DispatchQueue.main.async {
                    self.FinalRating.reloadData()
                }
            } catch{
                print("\(error)")
            }
        }.resume()
    }
}

//    extension FinalRatingViewController: UITableViewDataSource, UITableViewDelegate{
//        func tableView( _ tableView: UITableView, numberOfRowsInSection section:Int) -> Int {
//            return temp.count
//        }
//        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//            let data = temp[indexPath.row]
//        }
//        guard let cell = FinalRating.dequeueReusableCell(withIdentifier: "Cell") as? FinalTableViewCell else {return UITableViewCell()}
//        
//            cell.Lblname.text = data.u_name
//            
//    }

