//
//  SharedData.swift
//  RatingKaryawan
//
//  Created by Poernomo Santoso  on 15/10/19.
//  Copyright © 2019 Immobi. All rights reserved.
//

import Foundation


struct GlobalVariable {
    
    static var idLogin = String() // buat nyimpen id
    static var idSekawan = String()
    static var userName = String() // buat nyimpen userName
    static var divisiName = String() // buat nyimpen divisi name buat ngerating
    static var divisiUserLog = String() // buat nyimpen divisi si user yg login
    
}

