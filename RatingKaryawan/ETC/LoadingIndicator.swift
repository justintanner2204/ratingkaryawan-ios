//
//  LoadingIndicator.swift
//  RatingKaryawan
//
//  Created by Poernomo Santoso  on 05/12/19.
//  Copyright © 2019 Immobi. All rights reserved.
//

import Foundation
import SVProgressHUD


func Loading() {
    
    SVProgressHUD.show()
    SVProgressHUD.setBackgroundColor(UIColor.black)
    SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.clear)
    SVProgressHUD.setFadeOutAnimationDuration(0.5)
    SVProgressHUD.setBorderColor(UIColor.white)
    SVProgressHUD.setForegroundColor(UIColor.white)
    //SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.dark)
    SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
    
}
